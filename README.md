# Colorization-Tensorflow
Colorization ECCV2016 paper (Colorful Image Colorization)

We are using this model for coloring Cohn Kanade Images Dataset.

## Installation
```bash
pip3 install tensorflow==1.5 scikit-image scipy numpy
```

## How to
1. Download pretrained model first from [here](https://drive.google.com/file/d/0B-yiAeTLLamRWVVDQ1VmZ3BxWG8/view?usp=sharing)
2. move into models/
3. edit in demo.py
```python
# change the name
img = cv2.imread('S005_001_00000001.png')
```
4. run python3 demo.py

## Results
![alt text](results/concat.png)
